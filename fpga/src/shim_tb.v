`timescale 1ns/1ps



module shim_tb;

reg     clk         = 1'b1;
reg     spi_clk     = 1'b0;
reg     spi_csn     = 1'b1;
reg     spi_mosi    = 1'b0;
reg     [399:0] shifter;
wire    leds_din;

wire    [399:0] test_data = 400'h1111_2345_55aa23_000000_000000_000000_000000_000000_000000_000000_000000_000000_000000_000000_000000_000000_000000_00;

always
    #41.6666667 clk <= ~clk;



`define SPI_PERIOD      100
`define SPI_WORDGAP     (`SPI_PERIOD * 30)
`define SPI_MSGGAP      18000000
`define SPI_WORDS       25
`define SPI_WORDSIZE    16

integer i,j;
always
begin
    #5000;
    spi_csn <= 1'b0;
    shifter <= test_data;
    for(j=0; j<`SPI_WORDS; j=j+1)
    begin
        #(`SPI_WORDGAP);
        for(i=0; i<`SPI_WORDSIZE; i=i+1)
        begin
            spi_mosi <= shifter[399];
            shifter <= {shifter[398:0],1'b0};
            #(`SPI_PERIOD/2)    spi_clk <= 1'b1;
            #(`SPI_PERIOD/2)    spi_clk <= 1'b0;
        end
    end
    #1000;
    spi_csn <= 1'b1;
    #(`SPI_MSGGAP);
end

reg     fifo_clk = 1'b1;
always #16.6666667 fifo_clk <= ~fifo_clk;

wire    [7:0]   adbus;
reg     [7:0]   camdata = 0;
reg             hsync = 0;
reg             vsync = 0;
reg            txen = 1;
reg            atxen = 1;
reg             pclk = 0;

`define PCLK_PERIOD     83

`define LINES 200
integer h,v;
always
begin
    #2000;
    vsync <= 1'b0;
    hsync <= 1'b0;
    for(v=0; v<`LINES; v=v+1)
    begin
        if (v>2 && v<=`LINES-98)
            vsync <= 1'b1;
        else
            vsync <= 1'b0;
        for(h=0; h<800; h=h+1)
        begin
            if (h>100 && h<=740)
                hsync <= 1'b1;
            else
                hsync <= 1'b0;
            #(`PCLK_PERIOD/2)   pclk <= 1'b1;
            #(`PCLK_PERIOD/2)   pclk <= 1'b0;
            if (hsync & vsync)
                camdata <= camdata + 1;
        end
    end
end

always
begin
    #220000;
    atxen <= 1'b0;
    while(1)
    begin
        #20000;
        atxen <= 1'b1;
        #2000;
        atxen <= 1'b0;
    end
end
always @(posedge fifo_clk)
    txen <= atxen;

shim U_shim(
    .usb_clk    (clk),
    .xu4_spi1_clk   (spi_clk),
    .xu4_spi1_csn   (spi_csn),
    .xu4_spi1_mosi  (spi_mosi),
    .leds_din   (leds_din),

    .epuck_cam_clk  (),

    .fifo_clk   (fifo_clk),
    .adbus0     (adbus[0]),
    .adbus1     (adbus[1]),
    .adbus2     (adbus[2]),
    .adbus3     (adbus[3]),
    .adbus4     (adbus[4]),
    .adbus5     (adbus[5]),
    .adbus6     (adbus[6]),
    .adbus7     (adbus[7]),
    .rxfn       (),
    .txen       (txen),
    .rdn        (),
    .wrn        (),
    .oen        (),
    .siwu       (),

    .epuck_pclk (pclk),
    .epuck_vsync(vsync),
    .epuck_href (hsync),
    .epuck_y0   (camdata[0]),
    .epuck_y1   (camdata[1]),
    .epuck_y2   (camdata[2]),
    .epuck_y3   (camdata[3]),
    .epuck_y4   (camdata[4]),
    .epuck_y5   (camdata[5]),
    .epuck_y6   (camdata[6]),
    .epuck_y7   (camdata[7])

);

initial
begin
    //$dumpfile("shim.vcd");
    $dumpvars(0, shim_tb);
    # 2000000 $finish;
end


endmodule

