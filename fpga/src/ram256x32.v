
// 256 x 32 DP RAM
module ram256x32 #(
    parameter
    addr_width = 8,
    data_width = 32
)
(
    input                           wclk,
    input                           write_en,
    input       [addr_width-1:0]    waddr,
    input       [data_width-1:0]    din,

    input                           rclk,
    input                           read_en,
    input       [addr_width-1:0]    raddr,
    output reg  [data_width-1:0]    dout
);


reg [data_width-1:0] b0mem0 [255:0];


always @(posedge wclk)
begin
    if (write_en)
    begin
        b0mem0[waddr] <= din;
    end
end
always @(posedge rclk)
begin
    if (read_en)
    begin
        dout <= b0mem0[raddr];
    end
end

//integer i;
//initial
//    for(i=0;i<(1<<addr_width);i=i+1)
//        $dumpvars(0, memory[i]);
endmodule


