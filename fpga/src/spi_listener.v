//
// SPI listener module
//
// Watch the SPI data going past and stuff it into a fifo.
// Pull the data out and send it as the correct format bitstream to the LEDs
//

`define MSG_LEN 50*8

module spi_listener (
    input                       clk,
    input                       spi_clk,
    input                       spi_csn,
    input                       spi_mosi,
    output reg                  led_data
);





reg [8:0]               spi_count;
reg [23:0]              spi_data;
reg                     spi_done, done1, done2, done3;



// Main shifter and bit counter, in SPI clock domain
always @(posedge spi_clk or posedge spi_csn)
begin
    if (spi_csn)
    begin
        spi_count   = 0;
        spi_data    = 0;
    end
    else
    begin
        spi_count   = spi_count + 1;
        spi_data    = {spi_data[22:0],spi_mosi};
    end
end


reg [3:0]   inledcount;
reg         din_valid;
always @*
begin
    din_valid   = 1'b0;
    inledcount  = 4'h0;
    case (spi_count)
        9'h38:  begin din_valid = 1'b1; inledcount = 4'h0; end
        9'h50:  begin din_valid = 1'b1; inledcount = 4'h1; end
        9'h68:  begin din_valid = 1'b1; inledcount = 4'h2; end
        9'h80:  begin din_valid = 1'b1; inledcount = 4'h3; end
        9'h98:  begin din_valid = 1'b1; inledcount = 4'h4; end
        9'hb0:  begin din_valid = 1'b1; inledcount = 4'h5; end
        9'hc8:  begin din_valid = 1'b1; inledcount = 4'h6; end
        9'he0:  begin din_valid = 1'b1; inledcount = 4'h7; end
        9'hf8:  begin din_valid = 1'b1; inledcount = 4'h8; end
        9'h110: begin din_valid = 1'b1; inledcount = 4'h9; end
        9'h128: begin din_valid = 1'b1; inledcount = 4'ha; end
        9'h140: begin din_valid = 1'b1; inledcount = 4'hb; end
        9'h158: begin din_valid = 1'b1; inledcount = 4'hc; end
        9'h170: begin din_valid = 1'b1; inledcount = 4'hd; end
        9'h188: begin din_valid = 1'b1; inledcount = 4'he; end
    endcase
    
end


wire    [31:0]  din = {4'h0, inledcount, spi_data};
wire    [31:0]  dout;
wire            dout_valid;
reg             dout_enable;
fifo256x32 U_fifo (
    .din_clk        (spi_clk),
    .din_valid      (din_valid),
    .din_enable     (),
    .din            (din),
    .dout_clk       (clk),
    .dout_valid     (dout_valid),
    .dout_enable    (dout_enable),
    .dout           (dout)
);









//---------------------------------------------------------------------
// LED control
//---------------------------------------------------------------------
`define     CYCLE1      4
`define     RESETTICKS  1200
`define     NUM_LEDS    15
`define     WS          24

wire    [399:0]     all_msg_data;
wire    [359:0]     led_msg_data;
reg [2:0]   state,      n_state;
reg [3:0]   counter,    n_counter;
reg [23:0]  shifter,    n_shifter;
reg [4:0]   bitcount,   n_bitcount;
reg [3:0]   ledcount,   n_ledcount;

reg [`WS-1:0]   leddata;



// Each bit is sent encoded with a three cycle state
// 0    - HLL
// 1    - HHL
// where each state is ~400ns
// Each clock cycle at 12MHz is 83.33333 ns
//



// Neopixel drive state machine
always @*
begin
    dout_enable = 1'b0;
    n_state     = state;
    n_counter   = counter;
    n_shifter   = shifter;
    n_bitcount  = bitcount;
    n_ledcount  = ledcount;

    led_data    = 1'b0;
    case (state)
        2'h0:
        begin
            if (dout_valid)
                // Check if we are at the beginning of the sequence, if not,
                // don't change state, just suck data
                if (dout[31:24] == 8'h00)
                begin
                    n_state     = 2'b1;
                    n_shifter   = dout;
                    dout_enable = 1'b1;
                    n_counter   = 3'b0;
                    n_bitcount  = 4'b0;
                    n_ledcount  = 3'b1;
                end
                else
                    dout_enable = 1'b1;
        end
        2'h1:
        begin
            led_data    = 1'b1;
            n_counter   = counter + 1;
            if (counter == `CYCLE1)
            begin
                n_state     = 2'h2;
                n_counter   = 3'b0;
            end
        end
        2'h2:
        begin
            led_data    = shifter[23];
            n_counter   = counter + 1;
            if (counter == `CYCLE1)
            begin
                n_state     = 2'h3;
                n_counter   = 3'b0;
            end
        end
        2'h3:
        begin
            n_counter   = counter + 1;
            if (counter == `CYCLE1)
            begin
                n_shifter   = {shifter[22:0],1'b0};
                n_bitcount  = bitcount + 1;
                n_state     = 2'h1;
                n_counter   = 3'b0;
                if (bitcount == 5'h17)
                begin
                    n_bitcount  = 5'b0;
                    if (ledcount == `NUM_LEDS)
                    begin
                        n_ledcount  = 3'h0;
                        n_state     = 2'h0;
                    end
                    else
                    begin
                        n_ledcount  = ledcount + 1;
                        n_shifter   = dout;
                        // We assume the data will always arrive..
                        dout_enable = 1'b1;
                    end
                end
            end
        end
    endcase
end


always @(posedge clk)
begin
    state       <= n_state;
    counter     <= n_counter;
    shifter     <= n_shifter;
    bitcount    <= n_bitcount;
    ledcount    <= n_ledcount;
end

// This only matters for simulation. By default, registers power on reset to
// zero
initial
begin
    // Reset registers
    state       = 0;
    counter     = 0;
    shifter     = `RESETTICKS - 10;
    bitcount    = 0;
    ledcount    = 0;
end





endmodule

