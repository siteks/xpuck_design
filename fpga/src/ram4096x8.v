
// 4096 x 8 DP RAM, made up of two banks of four 2048x2 inferred 
// block RAMS
module ram4096x8 #(
    parameter
    addr_width = 12,
    data_width = 8
)
(
    input                           wclk,
    input                           write_en,
    input       [addr_width-1:0]    waddr,
    input       [data_width-1:0]    din,

    input                           rclk,
    input                           read_en,
    input       [addr_width-1:0]    raddr,
    output reg  [data_width-1:0]    dout
);


reg [1:0] b0mem0 [2047:0];
reg [1:0] b0mem1 [2047:0];
reg [1:0] b0mem2 [2047:0];
reg [1:0] b0mem3 [2047:0];
reg [1:0] b1mem0 [2047:0];
reg [1:0] b1mem1 [2047:0];
reg [1:0] b1mem2 [2047:0];
reg [1:0] b1mem3 [2047:0];


wire b0_write_en    = write_en & ~waddr[11];
wire b1_write_en    = write_en &  waddr[11];
wire b0_read_en     = read_en  & ~raddr[11];
wire b1_read_en     = read_en  &  raddr[11];

always @(posedge wclk)
begin
    if (b0_write_en)
    begin
        b0mem0[waddr[10:0]] <= din[1:0];
        b0mem1[waddr[10:0]] <= din[3:2];
        b0mem2[waddr[10:0]] <= din[5:4];
        b0mem3[waddr[10:0]] <= din[7:6];
    end
    if (b1_write_en)
    begin
        b1mem0[waddr[10:0]] <= din[1:0];
        b1mem1[waddr[10:0]] <= din[3:2];
        b1mem2[waddr[10:0]] <= din[5:4];
        b1mem3[waddr[10:0]] <= din[7:6];
    end
end
always @(posedge rclk)
begin
    if (b0_read_en)
    begin
        dout[1:0] <= b0mem0[raddr[10:0]];
        dout[3:2] <= b0mem1[raddr[10:0]];
        dout[5:4] <= b0mem2[raddr[10:0]];
        dout[7:6] <= b0mem3[raddr[10:0]];
    end
    if (b1_read_en)
    begin
        dout[1:0] <= b1mem0[raddr[10:0]];
        dout[3:2] <= b1mem1[raddr[10:0]];
        dout[5:4] <= b1mem2[raddr[10:0]];
        dout[7:6] <= b1mem3[raddr[10:0]];
    end
end

//integer i;
//initial
//    for(i=0;i<(1<<addr_width);i=i+1)
//        $dumpvars(0, memory[i]);
endmodule


